﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class FirePulse : NetworkBehaviour {

    public GameObject PulsePrefab;
    public Transform PulseSpawnLUp;
    public Transform PulseSpawnLDown;
    public Transform PulseSpawnRUp;
    public Transform PulseSpawnRDown;

    public float PulseVelocity = 100;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        // If not local player, return
        if (!isLocalPlayer)
            return;

        if (Input.GetKeyDown(KeyCode.U))
        {
            CreateFirePulse(1);
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            CreateFirePulse(2);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            CreateFirePulse(4);
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            CreateFirePulse(3);
        }
    }

    public void CreateFirePulse(int positionID)
    {
        // Create the Bullet from the Bullet Prefab
        Transform transform = PulseSpawnLUp;

        switch (positionID)
        {
            case 1:
                transform = PulseSpawnLUp;
                break;

            case 2:
                transform = PulseSpawnLDown;
                break;

            case 3:
                transform = PulseSpawnRDown;
                break;

            case 4:
                transform = PulseSpawnRUp;
                break;
        }


        var PulseIns = (GameObject)Instantiate(PulsePrefab, transform.position, transform.rotation);

        // Add velocity to the bullet
        PulseIns.GetComponent<Rigidbody>().velocity = PulseIns.transform.forward * PulseVelocity;

        // Destroy the bullet after 2 seconds
        Destroy(PulseIns, 5.0f);
    }


}
