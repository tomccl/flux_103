﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

    [SerializeField] public GameObject ScoreBoard;
    public bool IsAI = true;

    AudioSource PulseAudio;
    AudioSource HitAudio;
    AudioSource AIAudio;


    // Use this for initialization
    void Start ()
    {
        ScoreBoard = GameObject.Find("LevelScoreBoard");
        PulseAudio = GetComponent<AudioSource>();

        HitAudio = GameObject.Find("PlayerBox").GetComponent<AudioSource>();
        AIAudio = GameObject.Find("AIBox").GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "AI")
        {
            ScoreBoard.GetComponent<GameScore>().GameTotalScore++;
            HitAudio.Play(0);
        }

        if (collision.gameObject.tag == "Player")
        {
            ScoreBoard.GetComponent<GameScore>().GameOpponentScore++;
            AIAudio.Play(0);
        }

        if (collision.gameObject.tag == "Pulse")
        {
            PulseAudio.Play(0);
            Debug.Log(gameObject.name);
        }

        Destroy(gameObject.GetComponent<BoxCollider>(), 0.0f);
        Destroy(gameObject.GetComponent<MeshRenderer>(), 0.0f);
        Destroy(gameObject.GetComponent<Rigidbody>(), 0.0f);
        Destroy(gameObject.GetComponent<ParticleSystem>(), 0.0f);
        Destroy(gameObject, 1.0f);
    }


}
