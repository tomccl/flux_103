﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameScore : MonoBehaviour {

    public int GameTotalScore = 0;
    public int GameOpponentScore = 0;
    
    // Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        gameObject.GetComponent<UnityEngine.UI.Text>().text = (GameTotalScore.ToString() + " VS " + GameOpponentScore.ToString());
    }
}
